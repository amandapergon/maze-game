const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];

const mazeBox = document.getElementById('mazeBox')

function drawMazeGrid() {
    let blocks
    for (let i = 0; i < map.length; i++) {
        blocks = map[i].split("")
        for (let j = 0; j < blocks.length; j++) {
            if (blocks[j] == "W") {
                let wall = document.createElement('div')
                wall.className = "wall"
                mazeBox.appendChild(wall)
            } else if (blocks[j] == " ") {
                let way = document.createElement('div')
                way.className = "way"
                mazeBox.appendChild(way)
            } else if (blocks[j] == "S") {
                let start = document.createElement('div')
                start.className = "start"
                mazeBox.appendChild(start)
            } else if (blocks[j] == "F") {
                let end = document.createElement('div')
                end.className = "end"
                mazeBox.appendChild(end)
            }
        }
    }
}
drawMazeGrid()

let mazeTop = 566;
let mazeLeft = 278;
let player = document.getElementById("player")
let axisY = 9
let axisX = 0

document.addEventListener('keydown', (event) => {

    const keyName = event.key;

    switch (keyName) {
        case 'ArrowDown':
            if (map[axisY + 1][axisX] == " ") {
                axisY++
                mazeTop += 62
                player.style.top = mazeTop + "px"
            }
            break;
        case 'ArrowUp':
            if (map[axisY - 1][axisX] == " ") {
                axisY--
                mazeTop -= 62
                player.style.top = mazeTop + "px"
            }
            break;
        case 'ArrowLeft':
            if (map[axisY][axisX - 1] == " ") {
                axisX--
                mazeLeft -= 62
                player.style.left = mazeLeft + "px"
            }
            break;
        case 'ArrowRight':
            if (map[axisY][axisX + 1] == " " || map[axisY][axisX + 1] == "F") {
                axisX++
                mazeLeft += 62
                player.style.left = mazeLeft + "px"
            }
            break;
    }

    if (axisY == 8 && axisX == 20) {
        alert("Você venceu!")
    }

    console.log('keydown event\n\n' + 'key: ' + keyName);
});